//import { HttpClient } from '@angular/common/http';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
/*
  Generated class for the MovieProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable() // e pra ser usada dentro de outra classe
export class MovieProvider {
  private baseApiPath = "https://api.themoviedb.org/3";

  constructor(public http: Http) {
    console.log('Hello MovieProvider Provider');
  }

  getLatestMovies(page = 1) {
    return this.http.get( this.baseApiPath + `/movie/popular?page=${page}&api_key=10a550cec263b1a3289bb8e5f12dbf58`) ;
  }

  getMoviesDetails(filmeid) {
    return this.http.get(this.baseApiPath + `/movie/${filmeid}?api_key=10a550cec263b1a3289bb8e5f12dbf58`) ;
  }

  getApiKey() {
    return ("10a550cec263b1a3289bb8e5f12dbf58");
  }
 

}
