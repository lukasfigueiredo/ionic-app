import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MovieProvider } from '../../providers/movie/movie';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { FilmeDetalhesPage } from '../filme-detalhes/filme-detalhes'; 

/**
 * Generated class for the FeedPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feed',
  templateUrl: 'feed.html',
  providers: [
    MovieProvider
  ]
})
export class FeedPage {
 public objeto_feed = {
   titulo: "Lucas Figueiredo",
   data: "Novembro 10, 2017",
   descricao: "Estou criando um app incrivel...",
   qntd_likes: 12,
   qntd_comments: 4,
   time_comment: "11h ago"
 }

 public lista_filmes = new Array<any>();
 public page = 1;
 public infiniteScroll;

 public nome_usuario:string = "Lucas Figueiredo";
 public loader;
 public refresher;
 public isRefreshing: boolean = false;

  constructor(
     public navCtrl: NavController,
     public navParams: NavParams,
     private movieProvider: MovieProvider,
     public loadingCtrl: LoadingController
    ) {
  }


  abreCarregando() {
    this.loader = this.loadingCtrl.create({
      content: "Por favor! Aguarde ...",
     // duration: 3000
    });
    this.loader.present();
  }

  fecharCarregando(){
    this.loader.dismiss();
  }



  public somaDoisNumeros(num1:number, num2:number): void {
    alert(num1 + num2);
  }

  doRefresh(refresher) {
    this.refresher = refresher;
    this.isRefreshing = true;

    this.abreCarregando
  }

  ionViewDidEnter() {
    //console.log('ionViewDidLoad FeedPage');
   // this.somaDoisNumeros(5,15);
    this.carregarFilmes();
  }

  abrirDetalhes(filme){
    console.log(filme);
    this.navCtrl.push(FilmeDetalhesPage, { id: filme.id });
  }

  doInfinite(infiniteScroll) {
    this.page++;
    this.infiniteScroll = infiniteScroll;
    this.carregarFilmes(true);
  }


  carregarFilmes(newpage: boolean = false) {
    this.abreCarregando();
    this.movieProvider.getLatestMovies(this.page).subscribe(
       data=>{
         const response = (data as any);
         const objeto_retorno = JSON.parse(response._body);

          if(newpage) {
            this.lista_filmes = this.lista_filmes.concat(objeto_retorno.results);
            console.log(this.page);
            console.log(this.lista_filmes);
            this.infiniteScroll.complete();
          } else {
            this.lista_filmes = objeto_retorno.results;            
          }



         //console.log(objeto_retorno);
 
         this.fecharCarregando();
         if(this.isRefreshing){
           this.refresher.complete();
           this.isRefreshing = false;
         }
       }, error=>{  
         console.log(error);
         this.fecharCarregando();
       }
    )
  }



}
